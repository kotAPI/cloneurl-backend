const express = require('express');
const router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
const image_controller = require('../controllers/image.controller');


// a simple test url to check that all of our files are communicating correctly.
router.post('/', image_controller.image_create);
router.get('/',image_controller.images_get)

router.get('/:id',image_controller.get_image_details)
router.delete("/:id",image_controller.delete_image)
module.exports = router