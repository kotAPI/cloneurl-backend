const Image = require('../models/image.model');
var cloudinary = require('cloudinary');
cloudinary.config({ 
    cloud_name: 'dmwbjqt0l', 
    api_key: '295775288241226', 
    api_secret: 'RlHEN9m1giv1U88t3z5-XFHst5c' 
  });
//Simple version, without validation or sanitation


// get an image by id
exports.images_get = function (req, res,next) {
    Image.find({}, function(err, images) {
        if(err){
            return next(err)
        }
        var imgMap = [];
        images.forEach(function(image) {
            imgMap.push(image);
        });
        res.send(imgMap);  
      });
};

// listing all available images
exports.get_image_details =  function (req, res,next) {
    console.log(req.params)
    Image.findById(req.params.id, function (err, image) {
        if (err) return next(err);
        res.send(image);
    })
};


exports.delete_image = function(req,res,next){
    Image.findByIdAndRemove(req.params.id, function (err) {
        if (err) return next(err);
        res.send('Deleted successfully!');
    })
}


// controller to create an image entry
exports.image_create = function (req, res,next) {


   console.log("Entered create")
    var base64Data = req.body.image.replace(/^data:image\/png;base64,/, "");

    const shortid = require('shortid');
    var filename = shortid.generate()

    require("fs").writeFile("public/"+filename+".png", base64Data, 'base64', function(err) {
        console.log(err);
    });
   

    var path = require('path');
    var appDir = path.dirname(require.main.filename);
    var url = appDir+"/public/"+filename+".png"




    console.log("Cloudinary firing")
    cloudinary.v2.uploader.upload(url,
        function(error, result) {
            console.log(error,result)
            if(error){
                return next(error)
            }
            if(result){
                let image = new Image(
                    {
                        name: req.body.name,
                        url: result.secure_url,
                        description:req.body.description
                    }
                );
            
                image.save(function (err) {
                    if (err) {
                        return next(err);
                    }
                    res.send('Image Created successfully')
                })
            }
        }
    );



   


    
};