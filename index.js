// app.js
let port =  process.env.PORT || 8080;

const express = require('express');
const bodyParser = require('body-parser');
// initialize our express app
const app = express();
const cors = require('cors')
const ROOT = process.cwd()

const mongoose = require('mongoose');
let dev_db_url = 'mongodb://kotapi:30sToVenus@ds111319.mlab.com:11319/kotapi';
let mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(bodyParser.json({limit: '10mb', extended: true}))
app.use(bodyParser.urlencoded({limit: '10mb', extended: true}))
app.use(cors());
app.use(express.static('public'))

app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
});


// Routes
const image = require(ROOT+"/routes/image.route"); // Imports routes for the products
app.use('/image',image)