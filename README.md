# Backend

#### Folder structure
-controllers

-models

-public

-routes

index.js

package.json


#### Database
Using mongodb for storing data, the database is hosted on mongolabs. ODM- Mongoose

#### Models

ImageModel - 

    url: `<url for the image>`
    
    name: `<name for the image>`
    
    description: `<description for the image>`
    


### APIs for images
*POST* - you can add a `name` , `description` and a `file`(image) to create a new image entry.

- After a post request is made, the sent image is stored in a folder temporarily on the backend for image processing (adding watermark) which was done by a node package called `dynamic-watermark` One problem with the node packages are- they depend on lower level binaries for image processing to work, in this case ImageMagick and other binaries were required, this was tricky to get done on Heroku, I also tried a build pack for Heroku suggested by Arjun but there was no luck, it works well on local machine if binaries are available.Removed the watermarking feature for now to run on Heroku. The processed image is further uploaded to `cloudinary` bucket which in turn responds with the destination URL of the uploaded image. The returned image link is stored as a url field along with the name and description provided by the user.

*GET* - You can do a get on a particular image, and view its full details/data. Clicking on any image on the front end takes you to a full view of that image displaying its name and description etc.

You can also do a get list of user uploaded images. Which can be viewed on the front end by clicking `view images` or going to /user route.

*DELETE* - You can delete an image by going to the full view of the image on the front end and click delete, this removes the field from database completely